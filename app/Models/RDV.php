<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RDV extends Model
{
    protected $table = "RDV";
	// TIMESTAMPS: Laravel is expecting created_at and updated_at
    public $timestamps = false;

    protected $fillable=['dateHeure', 'idMDM', 'idAgent', 'idUsager', 'idObjet', 'idDomaine'];
}
