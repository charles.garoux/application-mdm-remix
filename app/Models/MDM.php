<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MDM extends Model
{
	protected $table = "MDM";
    // TIMESTAMPS: Laravel is expecting created_at and updated_at
    public $timestamps = false;

    protected $fillable=['nom', 'adresse', 'domiciliation'];
}