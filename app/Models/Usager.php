<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usager extends Model
{
	protected $table = "Usager";
    // TIMESTAMPS: Laravel is expecting created_at and updated_at
    public $timestamps = false;

    protected $fillable=['civilité', 'nom', 'prénom', 'adresse', 'domiciliation', 'dateDeNaissance', 'numéroDeTéléphone', 'email', 'listRouge', 'idMDM', 'idAgent'];
}
