<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domaine extends Model
{
    protected $table = "Domaine";
    // TIMESTAMPS: Laravel is expecting created_at and updated_at
    public $timestamps = false;

    protected $fillable=['nom', 'libellé'];
}
