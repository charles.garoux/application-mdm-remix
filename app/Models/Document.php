<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
	protected $table = "Document";
	// TIMESTAMPS: Laravel is expecting created_at and updated_at
    public $timestamps = false;

    protected $fillable=['dateDeDépôt', 'dateDeRetrait', 'idUsager', 'idAgentDépôt', 'idAgentRetrait'];
}
