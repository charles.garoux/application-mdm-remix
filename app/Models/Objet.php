<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Objet extends Model
{
    protected $table = "Objet";
    // TIMESTAMPS: Laravel is expecting created_at and updated_at
    public $timestamps = false;

    protected $fillable=['nom', 'libellé'];
}
