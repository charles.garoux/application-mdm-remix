<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = "Contact";
	// TIMESTAMPS: Laravel is expecting created_at and updated_at
    public $timestamps = false;

    protected $fillable=['dateHeure', 'modeDeContact','idAgent', 'idUsager', 'idMDM', 'idDomaine', 'idObjet'];
}
