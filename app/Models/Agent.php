<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
	protected $table = "Agent";
    // TIMESTAMPS: Laravel is expecting created_at and updated_at
    public $timestamps = false;

    protected $fillable=['identifiant', 'motDePasse', 'civilité', 'nom', 'prénom', 'idMDM'];
}
