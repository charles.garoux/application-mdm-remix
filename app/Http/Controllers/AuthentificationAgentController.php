<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Models\Agent;

class AuthentificationAgentController extends Controller
{
    public function formulaire()
    {
    	return view("authentification");
    }

    public function vérification(Request $request)
    {

    	//=============================================
        //Les mot de passe devront être hashé,
        //mais il faut d'abord faire un administrateur
        //qui ma créer les compte agent et qui hashera
        //les mot de passe.
        //=============================================

    	$identifiant = $request->input('identifiant');
    	$motDePasse = $request->input('motDePasse');

    	$motDePasseHashé = Agent::select('motDePasse')->where('identifiant', $identifiant)->first()->motDePasse;

        if($motDePasseHashé == null)
        {
            Session::flash('erreur', "L'identifiant est incorrecte.");
            return back();
        }

    	//if(Hash::check($motDePasse, $motDePasseHashé))// finale
    	if($motDePasseHashé == $motDePasse) //temporaire
		{
			$Agent = Agent::select('id','nom','prénom', 'idMDM')->where('identifiant', $identifiant)->first();
			Session::put('Agent', $Agent);
			Session::flash('succès', "Bonjour " . $Agent->nom . " " . $Agent->prénom);
			return redirect(route('application.agent.volant.menu'));
		}
        Session::flash('erreur', "Le mot de passe est incorrecte.");
        return back();
    }

    public function déconnexion()
    {
    	Session::flush();
        return redirect()->route('application.agent.authentification.formulaire');
    }

}
