<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Contact;
use App\Models\Domaine;
use App\Models\Objet;


class ContactController extends Controller
{
    static function création_contact($idAgent, $idUsager, $idMDM, $nomDomaine, $nomObjet)
    {
    	$Domaine = Domaine::select('id')->where('nom', $nomDomaine)->first();

        if ($Domaine == null)
        {
            Session::flash('erreur', "Le domaine pour le contact n'éxiste pas.");
            return back();
        }

    	$Objet = Objet::select('id')->where('nom', $nomObjet)->first();

        if ($Objet == null)
        {
            Session::flash('erreur', "L'objet pour le contact n'éxiste pas.");
            return back();
        }

    	Contact::create([
            "dateHeure" => now(),
            "modeDeContact" => "Physique",
            "idAgent" => $idAgent,
            "idUsager" => $idUsager,
            "idMDM" => $idMDM,
            "idDomaine" => $Domaine->id,
            "idObjet" => $Objet->id
        ]);
    }
}