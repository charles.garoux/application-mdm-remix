<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ContactController;
use Illuminate\Support\Facades\Session;
use App\Models\MDM;
use App\Models\Agent;
use App\Models\Usager;
use App\Models\Document;

class DocumentController extends Controller
{

    function menu()
    {
        return view('document.menu_document');
    }

    function formulaire_dépôt()
    {
    	return view('document.dépot_document');
    }

    function traitement_dépôt(Request $request) 
    {
        //=============================================
        //Envoie de SMS quand enregistré (si possible).
        //=============================================

    	$nom = $request->input('nom');
    	$prénom = $request->input('prénom');

        $Usager = Usager::select('id')->where('nom', $nom)->where('prénom', $prénom)->first();

        if ($Usager == null)
        {
            Session::flash('erreur', "L'usager " . $nom . " " . $prénom . " n'existe pas.");
            return back();
        }

        $Document = Document::create([
            "dateDeDépôt" => now(),
            "idUsager" => $Usager->id,
            "idAgentDépôt" => Session::get('Agent')->id
        ]);

        if ($Document == null)
        {
            Session::flash('erreur', "Le document n'a pas pu être enregistré.");
            return back();
        }

        /*
        ** Envoie du SMS à l'usager.
        */

    	Session::flash('succès', "Document de " . $nom . " " . $prénom . " enregistré.");
    	return back();
    }

    function formulaire_recherche()
    {
        return view('document.recherche_document');
    }

    function redirection_recherche(Request $request)
    {
    	$nom = $request->input('nom');
        $prénom = $request->input('prénom');

        return redirect()->route('application.document.usager.liste', ['nom' => $nom, 'prenom' => $prénom]);
    }

    function liste_document($nom, $prénom)
    {
        $Usager = Usager::select('id')->where('nom', $nom)->where('prénom', $prénom)->first();

        if ($Usager == null)
        {
            Session::flash('erreur', "L'usager " . $nom . " " . $prénom . " n'existe pas.");
            return back();
        }

        $Usager->nom = $nom;
        $Usager->prénom = $prénom;

        $Documents = Document::where('idUsager', $Usager->id)->where('dateDeRetrait', null)->orderBy('dateDeDépôt', 'desc')->get();

        if ($Documents == null)
        {
            $Documents->nombre = 0;
            return view('document.résultat_recherche', compact('Documents', 'Usager'));
        }

        $Documents->nombre = count($Documents);
        return view('document.liste_document', compact('Documents', 'Usager'));    
    }

    function retrait_document($idDocument)
    {
		//=============================================
        //Ajouter l'id de l'agent qui l'a retiré.
        //=============================================

        $Document = Document::find($idDocument);

        if ($Document == null)
        {
            Session::flash('erreur', "Le document " . $Document . " n'a pas été trouvé.");
            return back();
        }

        $Document->dateDeRetrait = now();
        $Document->idAgentRetrait = Session::get('Agent')->id;
        $Document->save();

        $AgentSession = Session::get('Agent');
        ContactController::création_contact($AgentSession->id, $Document->idUsager, $AgentSession->idMDM, "Document", "Retrait de document");

        Session::flash('succès', "Le document a bien été signalé comme retiré.");
        return back();
    }

    function contact_document_sans_retrait(Request $request)
    {
        $idUsager = $request->input('idUsager');

        $AgentSession = Session::get('Agent');
        ContactController::création_contact($AgentSession->id, $idUsager, $AgentSession->idMDM, "Document", "Recherche de document sans retrait");

        Session::flash('succès', "Le contact a été enregistré.");

        return redirect()->route('application.agent.volant.menu');
    }
}
