<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ContactController;
use Illuminate\Support\Facades\Session;
use App\Models\Usager;
use App\Models\RDV;
use App\Models\Agent;
use App\Models\Domaine;
use App\Models\Objet;
use Carbon\Carbon;

class RDVController extends Controller
{
	function formulaire_recherche_RDV_usager()
    {
        return view('RDV.avec.recherche_RDV');
    }

    function traitement_recherche(Request $request)
    {
        //=============================================
        //Envoie du message à l'agent si RDV trouvé.
        //=============================================

    	$nom = $request->input('nom');
        $prénom = $request->input('prénom');
        $heureRDV = $request->input('heure');

        $Usager = Usager::where('nom', $nom)->where('prénom', $prénom)->first();

        if ($Usager == null)
        {
            Session::flash('erreur', "L'usager " . $nom . " " . $prénom . " n'existe pas.");
            return back();
        }

        $Usager->nom = $nom;
        $Usager->prénom = $prénom;

        $dateHeure = Carbon::createFromTimeString($heureRDV);

        $RDV = RDV::where('dateHeure', $dateHeure)->where('idUSager', $Usager->id)->first();
        
        if ($RDV == null)
        {
            Session::flash('Usager', $Usager);
            Session::flash('erreur', "Le rendez-vous de " . $heureRDV ." de " . $nom . " " . $prénom ." n'a pas été trouvé.");
            return redirect()->route('application.RDV.créneaux.liste', ['dateDebut' => now(), 'dateFin' => now()->modify('+1 month'), 'type' => "RDV"]);
        }

        $Agent = Agent::find($RDV->idAgent);

        if ($Agent == null)
        {
            Session::flash('erreur', "L'agent correspondant au rendez-vous n'éxiste pas.");
            return back();
        }

        $AgentSession = Session::get('Agent');
        if ($RDV->idAgent)
            ContactController::création_contact($AgentSession->id, $Usager->id, $AgentSession->idMDM, "RDV", "Arrivé pour RDV");
        else
            ContactController::création_contact($AgentSession->id, $Usager->id, $AgentSession->idMDM, "RDV", "Arrivé pour AIS");

        /*
        ** Envoie du message à l'agent référent
        */

        return view('RDV.avec.résultat_recherche_RDV', compact('RDV', 'Usager', 'Agent'));
    }

    function formulaire_recherche_RDV()
    {
        return view('RDV.recherche_créneaux_RDV');
    }

    function traitement_recherche_RDV(Request $request)
    {
        $dateDébut = $request->input('dateDébut');
        $dateFin = $request->input('dateFin');

        $dateHeureDébut = Carbon::createFromDate($dateDébut);
        $dateHeureFin = Carbon::createFromDate($dateFin);

        return redirect()->route('application.RDV.créneaux.liste', ['dateDebut' => $dateHeureDébut, 'dateFin' => $dateHeureFin, 'type' => "RDV" ]);
    }

    function liste_créneaux($dateDebut, $dateFin, $type)
    {
        $AgentSession = Session::get('Agent');

        if ($type != "RDV" && $type != "AIS")
        {
            Session::flash('erreur', $type . " n'est pas un type de rendez-vous correcte.");
            return back();
        }
        if ($dateDebut > $dateFin)
        {
            Session::flash('erreur', "La date de début est supérieur à la date de fin.");
            return back();
        }

        if ($type == "AIS")
            $RDVs = RDV::whereBetween('dateHeure', [$dateDebut, $dateFin])->where('dateHeure', '>', now())->where('idMDM', $AgentSession->idMDM)->where('idAgent', null)->where('idUsager', null)->orderBy('dateHeure', 'ASC')->get();
        else
        {
            $Usager = Session::get('Usager');
            if($Usager && $Usager->idAgent)
                $RDVs = RDV::whereBetween('dateHeure', [$dateDebut, $dateFin])->where('dateHeure', '>', now())->where('idMDM', $AgentSession->idMDM)->whereNotNull('idAgent')->where('idUsager', null)->where('idAgent', $Usager->idAgent)->orderBy('dateHeure', 'ASC')->get();
            else
            $RDVs = RDV::whereBetween('dateHeure', [$dateDebut, $dateFin])->where('dateHeure', '>', now())->where('idMDM', $AgentSession->idMDM)->whereNotNull('idAgent')->where('idUsager', null)->orderBy('dateHeure', 'ASC')->get();
        }

        if ($RDVs == null)
        {
            Session::flash('information', "Auncun RDV à été trouvé");
            return back();
        }

        $RDVs->nombre = count($RDVs);
        
        return view('RDV.liste_créneaux', compact('RDVs', 'type'));
    }

    function prise_de_RDV(Request $request)
    {
        $nom = $request->input('nom');
        $prénom = $request->input('prénom');
        $email = $request->input('email');
        $numéroDeTéléphone = $request->input('numéroDeTéléphone');
        $idRDV = $request->input('idRDV');
        $civilité = $request->input('civilité');
        
        $RDV = RDV::find($idRDV);

        if ($RDV == null)
        {
            Session::flash('erreur', "Le RDV correspondant n'éxiste pas.");
            return back();
        }

        $Usager = Usager::where('nom', $nom)->where('prénom', $prénom)->first();

        if ($Usager == null)
        {
            if ($civilité)
                $Usager = Usager::create([
                    "civilité" => $civilité,
                    "nom" => $nom,
                    "prénom" => $prénom,
                    "email" => $email,
                    "numéroDeTéléphone" => $numéroDeTéléphone,
                    "listRouge" => "non",
                    "idMDM" => $RDV->idMDM,
                    "idAgent" => $RDV->idAgent
                ]);
            else if ($RDV->idAgent)
                $Usager = Usager::create([
                    "nom" => $nom,
                    "prénom" => $prénom,
                    "email" => $email,
                    "numéroDeTéléphone" => $numéroDeTéléphone,
                    "listRouge" => "non",
                    "idMDM" => $RDV->idMDM,
                    "idAgent" => $RDV->idAgent
                ]);
            else
                $Usager = Usager::create([
                    "nom" => $nom,
                    "prénom" => $prénom,
                    "email" => $email,
                    "numéroDeTéléphone" => $numéroDeTéléphone,
                    "listRouge" => "non",
                    "idMDM" => null,
                    "idAgent" => null
                ]);
            Session::flash('succès', "L'usager " . $nom . " " . $prénom . " a été enregistré.");
        }
        else if ($civilité != $Usager->civilité || $nom != $Usager->nom ||
                    $prénom != $Usager->prénom || $email != $Usager->email ||
                    $numéroDeTéléphone != $Usager->numéroDeTéléphone)
        {
            $Usager->civilité = $civilité;
            $Usager->nom = $nom;
            $Usager->prénom = $prénom;
            $Usager->email = $email;
            $Usager->numéroDeTéléphone = $numéroDeTéléphone;
            $Usager->save();
        }

        $RDV->idUsager = $Usager->id;
        $RDV->save();

        if ($RDV->idAgent)
            $type = "RDV";
        else
            $type = "AIS";

        $AgentSession = Session::get('Agent');
        if ($RDV->idAgent)
        ContactController::création_contact($AgentSession->id, $Usager->id, $AgentSession->idMDM, "RDV", "Prise de RDV");
        else
            ContactController::création_contact($AgentSession->id, $Usager->id, $AgentSession->idMDM, "AIS", "Prise d'AIS");

        return redirect()->route('application.RDV.récapitulatif', [$idRDV, $type]);
    }

    function récapitulatif_RDV($idRDV, $type)
    {
        $RDV = RDV::find($idRDV);

        if ($RDV == null)
        {
            Session::flash('erreur', "Le RDV correspondant n'éxiste pas.");
            return back();
        }

        $Agent = Agent::find($RDV->idAgent);

        if ($type == "RDV" && $Agent == null)
        {
            Session::flash('erreur', "L'agent correspondant au rendez-vous n'éxiste pas.");
            return back();
        }

        $Usager = Usager::find($RDV->idUsager);

        if ($Usager == null)
        {
            Session::flash('erreur', "L'usager correspondant au rendez-vous n'éxiste pas.");
            return back();
        }

        return view('RDV.récapitulatif_RDV', compact('RDV', 'Agent', 'Usager'));
    }

}
