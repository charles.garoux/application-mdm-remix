<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\MDM;
use App\Models\Agent;
use App\Models\Usager;
use App\Models\RDV;

class Démonstration extends Controller
{
    function liste_formulaire()
    {
    	$MDMs = MDM::all();
    	$Agents = Agent::all();

    	return view('administration.liste_formulaire', compact('MDMs', 'Agents'));
    }

    function ajouter_RDV(Request $request)
    {
    	$date = $request->input('date');
    	$heure = $request->input('heure');
    	$type =  $request->input('type');
    	$idAgent = $request->input('idAgent');
    	$idMDM = $request->input('idMDM');

    	if ($type == "RDV")
	    	$RDV = RDV::create([
	    		'dateHeure' => $date . " " . $heure . ":00",
	    		'idMDM' => $idMDM,
	    		'idAgent' => $idAgent
	    	]);
	    else
	    	$RDV = RDV::create([
	    		'dateHeure' => $date . " " . $heure . ":00",
	    		'idMDM' => $idMDM,
	    	]);

	    Session::flash('succès', "Le RDV a été enregistré.");
    	return back();
    }

    function ajouter_usager(Request $request)
    {
    	$nom = $request->input('nom');
        $prénom = $request->input('prénom');
        $email = $request->input('email');
        $numéroDeTéléphone = $request->input('numéroDeTéléphone');
        $civilité = $request->input('civilité');

        $Usager = Usager::create([
            "civilité" => $civilité,
            "nom" => $nom,
            "prénom" => $prénom,
            "email" => $email,
            "numéroDeTéléphone" => $numéroDeTéléphone,
            "listRouge" => "non",
            "idMDM" => Session::get('Agent')->idMDM,
            "idAgent" => Session::get('Agent')->id
        ]);

        Session::flash('succès', "L'usager a été enregistré.");
    	return back();
    }

    function ajouter_MDM(Request $request)
    {
    	$nom = $request->input('nom');
        $adresse = $request->input('adresse');
        $domiciliation = $request->input('domiciliation');

    	$MDM = MDM::create([
            "nom" => $nom,
            "adresse" => $adresse,
            "domiciliation" => $domiciliation
        ]);

    	Session::flash('succès', "La MDM a été enregistré.");
    	return back();
    }

    function ajouter_agent(Request $request)
    {
    	$nom = $request->input('nom');
        $prénom = $request->input('prénom');
        $identifiant = $request->input('identifiant');
        $motDePasse = $request->input('motDePasse');
        $civilité = $request->input('civilité');
        $idMDM = $request->input('idMDM');

    	$Agent = Agent::create([
            "nom" => $nom,
            "prénom" => $prénom,
            "identifiant" => $identifiant,
            "motDePasse" => $motDePasse,
            "civilité" => $civilité,
            "idMDM" => $idMDM
        ]);

    	Session::flash('succès', "La agent a été enregistré.");
    	return back();
    }
}
