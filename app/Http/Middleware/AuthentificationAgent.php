<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;


class AuthentificationAgent
{
    public function handle($request, Closure $next)
    {
        if (Session::has('Agent'))
        {
            return $next($request);
        }
        return redirect()->route('application.agent.authentification.formulaire');
    }
}
