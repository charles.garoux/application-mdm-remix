# Contexte du projet
Un "[MDM remix]([https://www.erasme.org/MDM-Remix](https://www.erasme.org/MDM-Remix))" le 12 et 13 mars 2019 organisé par [Erasme]([https://www.erasme.org/](https://www.erasme.org/)).

Il s'agit d'aider les Maisons de la Métropole à trouver les solutions d'assistance sociale de demain.
Après ces deux jours, les idées qui ont émergé avait la possibilité d'être prototypées pendant deux semaines de workshop qui ont suivie.

Le projet était d'améliorer l'accueil des MDMs sur deux axes :
- Des équipements modulables pour les MDMs
- Une application pour que les agents d'accueils puissent les rendre mobiles

J'ai développé cette application durant le workshop.

# Objectif de l'application
L'application a pour objectif d'éviter à des usagers d'attendre dans la file d'attente pour des interactions rapides et de désengorger l'accueil dans les Maisons de la Métropole. 

Features prévus :
- Prise de rendez-vous
- Signaler l'arrivée d'un usager pour son rendez-vous
- Signaler l'arrivée d'un usager pour un AIS
- Signaler le dépôt de document pour un usager
- Connaître les documents qu'un usager peut retirer
- Retirer un document d'un usager
- Rediriger un usager qui aura besoin d'un service qui n'est pas proposé par la MDM
>>>
Pour le signalement de l'arrivé : l'agent recevra une notification de l'arrivée de l'usager sur son poste de travail.<br>
Pour le dépôt de document : envoyer une SMS à l'usager qui a un document à récupérer.<br>
Pour la redirection : le projet "Boussole" sera utilisé pour rediriger l'usager.<br>
>>>

# Résultat de 2 semaines de workshop

Un **script bash d'installation** pour l'application a été produit et mis sur [un dépôt publique](https://gitlab.com/charles.garoux/installeur).

Une **application web** avec le framework PHP Laravel. L'application n'a pas pu être terminé à temps.

Features intégrés:
- L'authentification de l'agent qui l'utilise
- Prise des rendez-vous avec une agent référent et pour un AIS
	> L'AIS a été fonctionne comme un rendez-vous
- Signalement de l'arrivé d'un usager pour une rendez-vous
	> Le système de notification n'a pas pu être fait à temps
- Signaler le dépôt de document pour un usager
	> Le système d'envoie de SMS n'a pas pu être fait à temps
- Connaître les documents qu'un usager pu retirer
- Retirer un document d'un usager
- Ajout d'agent, d'usager, de rendez-vous et de MDM pour faire les démonstrations
	> Cette partie la n'est là que pour faire la démonstration de l'application. Un agent connecté peut y accéder.
