@extends('includes.template')

@section('onglet-droit')
<div class="bg-info interface-onglet-droit">
	<p class=" text-white h4">Administration</p>
</div>
@endsection

@section('contenu')
<div class="col-9 offset-1 pt-5 justify-content-center interface-main">
	@include('includes.flash')
	<div class="col-10 offset-1 card bg-info shadow mb-3">
		<form action="{{ route('application.administration.ajout.RDV') }}" method="post" class="card-body">
			@csrf
			<input type="hidden" name="idAgent" value="{{ Session::get('Agent')->id }}">
			<h3>Ajout d'un RDV</h3>
			<div class="form-group col-6 offset-2">
				<label for="date">Date</label>
				<input type="date" class="form-control form-control-lg" name="date" id="date" required/>
			</div>
			<div class="form-group col-6 offset-2">
				<label for="heure">Heure</label>
				<input type="time" class="form-control form-control-lg" name="heure" id="heure" required/>
			</div>
			
			<div class="row">
				<div class="form-group col-3 offset-1">
					<select class="form-control" name="type">
						<option value="RDV">RDV</option>
						<option value="AIS">AIS</option>
					</select>
				</div>
				<div class="form-group col-3">
					<select class="form-control" name="idAgent">
						@foreach($Agents as $Agent)
						<option value="{{ $Agent->id }}">{{ $Agent->identifiant }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-4">
					<select class="form-control" name="idMDM">
						@foreach($MDMs as $MDM)
						<option value="{{ $MDM->id }}">{{ $MDM->nom }}</option>
						@endforeach
					</select>
				</div>
			</div>
			
			<div class="row py-2">
				<button class="btn btn-lg btn-dark offset-4 p-3"><span class="h4">Enregistrer le RDV</span></button>
			</div>
		</form>
	</div>
	<div class="col-10 offset-1 card bg-info shadow mb-3">
		<form action="{{ route('application.administration.ajout.usager') }}" method="post" class="card-body">
			@csrf
			<h3>Ajout d'un usager</h3>
			<div class="row">
				<div class="form-group col-2">
					<select type="text" class="form-control form-control-lg" name="civilité" id="civilité">
						<option value=""></option>
						<option value="M">M</option>
						<option value="M">Mme</option>
					</select>
				</div>
				<div class="form-group col-5">
					<input type="text" class="form-control form-control-lg" name="nom" id="nom" placeholder="Nom" maxlength="32" required/>
				</div>
				<div class="form-group col-5">
					<input type="text" class="form-control form-control-lg" name="prénom" id="prénom" placeholder="Prénom" maxlength="32" required/>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-6">
					<input type="text" class="form-control form-control-lg" name="numéroDeTéléphone" id="numéroDeTéléphone" placeholder="06 ** ** ** **" maxlength="10"/>
				</div>
				<div class="form-group col-6">
					<input type="email" class="form-control form-control-lg" name="email" id="email" placeholder="email{{"@"}}exemple.ex" maxlength="32"/>
				</div>
			</div>
			<div class="row py-2">
				<button class="btn btn-lg btn-dark offset-4 p-3"><span class="h4">Enregistrer l'usager</span></button>
			</div>
		</form>
	</div>
	<div class="col-10 offset-1 card bg-info shadow mb-3">
		<form action="{{ route('application.administration.ajout.agent') }}" method="post" class="card-body">
			@csrf
			<h3>Ajout d'agent</h3>
			<div class="row">
				<div class="form-group col-2">
					<select type="text" class="form-control form-control-lg" name="civilité" id="civilité">
						<option value="M">M</option>
						<option value="Mme">Mme</option>
					</select>
				</div>
		    	<div class="form-group col-5">
					<input type="text" class="form-control form-control-lg" name="nom" id="nom" placeholder="Nom" maxlength="32" required/>
				</div>
			    <div class="form-group col-5 offset">
					<input type="text" class="form-control form-control-lg" name="prénom" id="prénom" placeholder="Prénom" maxlength="32" required/>
				</div>
			</div>
			<div class="row">
			    <div class="col">
			    	<div class="form-group col offset">
						<input type="text" class="form-control form-control-lg" name="identifiant" id="identifiant" placeholder="Identifiant" maxlength="32" required/>
					</div>
			    </div>
			    <div class="form-group col offset">
					<input type="password" class="form-control form-control-lg" name="motDePasse" id="motDePasse" placeholder="Mot de passe" maxlength="32" required/>
				</div>
			</div>
			<div class="form-group offset-2 col-4">
				<select class="form-control" name="idMDM">
					@foreach($MDMs as $MDM)
					<option value="{{ $MDM->id }}">{{ $MDM->nom }}</option>
					@endforeach
				</select>
			</div>
			<div class="row py-2">
				<button class="btn btn-lg btn-dark offset-4 p-3"><span class="h4">Enregistrer l'agent</span></button>
			</div>
		</form>
	</div>
	<div class="col-10 offset-1 card bg-info shadow mb-3">
		<form action="{{ route('application.administration.ajout.MDM') }}" method="post" class="card-body">
			@csrf
			<h3>Ajout d'MDM</h3>
			<div class="form-group col-5 offset-2 pb-2">
				<input type="text" class="form-control form-control-lg" name="nom" id="nom" placeholder="Nom" maxlength="32" required/>
			</div>
			<div class="form-group col-7 offset-2  py-2">
				<input type="text" class="form-control form-control-lg" name="adresse" id="adresse" placeholder="Adresse" maxlength="32" required/>
			</div>
			<div class="form-group row">
				<label class="col-form-label col-3 offset-3" for="domiciliation">Domiciliation</label>
				<select class="form-control col-2" name="domiciliation">
					<option value="oui">oui</option>
					<option value="non">non</option>
				</select>
			</div>
			<div class="row py-2">
				<button class="btn btn-lg btn-dark offset-4 p-3"><span class="h4">Enregistrer la MDM</span></button>
			</div>
		</form>
	</div>
</div>
@endsection