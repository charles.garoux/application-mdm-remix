@extends('includes.template')

@section('onglet-gauche')
<div class="bg-primary interface-onglet-gauche-2 pt-4 pb-2 pr-3 pl-2 ">
	<a href="{{ route('application.RDV.créneaux.recherche.formulaire') }}" class="text-white h4">Prise<br>RDV</a></p>
</div>
@endsection

@section('onglet-droit')
	<div class="text-white interface-onglet-droit-2  pt-1 pb-1 px-2">
		<p class="text-center"><span class="h5"> {{ Session::get('Agent')->nom . " " . Session::get('Agent')->prénom }}</span><br>
			<a class="text-white" href="{{ route('application.agent.déconnexion') }}"><img class="pr-1" src="{{ asset("img/croix-déconnexion.svg") }}">Me déconnecter</a>
		</p>
		
	</div>
@endsection

@section('contenu')

<div class="row mt-7 mb-2">
	<div class="col-5 offset-1 mr-2 bg-primary rounded-lg">
		<a href="{{ route('application.RDV.formulaire.recherche.usager') }}">
			<img class="mt-4 mx-auto d-block" src="{{ asset("img/logos/avec-RDV.svg") }}">
			<p class="text-center text-white">Avec RDV</p>
		</a>
	</div>
	<div class="col-5 bg-warning rounded-lg">
		<a href="{{route('application.RDV.créneaux.liste', ['dateDebut' => now(), 'dateFin' => now()->modify('+1 day'), 'type' => "AIS" ]) }}">
			<img class="mt-4 mx-auto d-block" src="{{ asset("img/logos/sans-RDV.svg") }}">
			<p class="text-center text-white">Sans RDV</p>
		</a>
	</div>
</div>
<div class="row">
	<div class="col-5 offset-1 mr-2 bg-success rounded-lg">
		<a href="{{ route('application.document.menu') }}">
			<img class="mt-4 mx-auto d-block" src="{{ asset("img/logos/documents.svg") }}">
			<p class="text-center text-white">Retrait/Dépôt documents</p>
		</a>
	</div>
	<div class="col-5 bg-danger rounded-lg">
		<a href="{{-- route('') --}}">
			<img class="mt-4 mx-auto d-block" src="{{ asset("img/logos/réorientation.svg") }}">
			<p class="text-center text-white">Réorientation</p>
		</a>
	</div>
</div>
<div class="interface-onglet-déconnexion bg-transparent">
	
</div>
@endsection