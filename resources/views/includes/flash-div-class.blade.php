@if (Session::has("succès"))
    <div class="{{ $div_class }} alert alert-success" role="alert">
        {{ Session::get("succès") }}
    </div>
@endif
@if (Session::has("erreur"))
    <div class="{{ $div_class }} alert alert-danger" role="alert">
        {{ Session::get("erreur") }}
    </div>
@endif
@if (Session::has("information"))
    <div class="{{ $div_class }} alert alert-primary" role="alert">
        {{ Session::get("information") }}
    </div>
@endif
@if (Session::has("attention"))
    <div class="{{ $div_class }} alert alert-warning" role="alert">
        {{ Session::get("attention") }}
    </div>
@endif
