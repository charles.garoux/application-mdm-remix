@extends('includes.template')

@section('onglet-droit')
<div class="bg-success interface-onglet-droit">
	<img class="img-fluid mt-2 mx-auto d-block" src="{{ asset("img/logos/documents.svg") }}" width="60px" height="60px">
	<p class="text-white h4">Documents</p>
</div>
@endsection

@section('contenu')
<div class="row mt-8">
	<div class="col-5 offset-1 mr-2 bg-success rounded-lg">
		<a href="{{route('application.document.formulaire.recherche') }}">
			<img class="mt-4 mx-auto d-block" src="{{ asset("img/logos/retrait-document.svg") }}">
			<p class="text-center text-white">Retrait documents</p>
		</a>
	</div>
	<div class="col-5 bg-success rounded-lg">
		<a href="{{route('application.document.formulaire.dépôt') }}">
			<img class="mt-4 mx-auto d-block" src="{{ asset("img/logos/dépôt-document.svg") }}">
			<p class="text-center text-white">Dépôt documents</p>
		</a>
	</div>
</div>
@endsection