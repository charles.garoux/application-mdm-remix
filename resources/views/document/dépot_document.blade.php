@extends('includes.template')

@section('onglet-droit')
<div class="bg-success interface-onglet-droit">
	<img class="img-fluid mt-4 mx-auto d-block" src="{{ asset("img/logos/documents.svg") }}" width="60px" height="60px">
	<p class=" text-white h4">Documents</p>
</div>
@endsection

@section('contenu')
<form method="post" class="col-9 offset-1 pt-5 bg-success justify-content-center interface-main">
	@csrf
	<div class="form-group col-5 offset-2 pt-5 pb-2">
		<input type="text" class="form-control form-control-lg" name="nom" id="nom" placeholder="Nom" maxlength="32" required/>
	</div>
	<div class="form-group col-5 offset-2  py-2">
		<input type="text" class="form-control form-control-lg" name="prénom" id="prénom" placeholder="Prénom" maxlength="32" required/>
	</div>
	<div class="row py-2">
		<button class="btn btn-lg btn-dark interface-onglet-bas-droit p-3"><span class="h4">Enregistrer le document</span></button>
		@include('includes.flash-div-class', ['div_class' => 'mt-4 col-7 offset-4'])
	</div>
</form>
@endsection