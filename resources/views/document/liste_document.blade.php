@extends('includes.template')
@section('onglet-droit')
<div class="bg-success interface-onglet-droit">
	<img class="img-fluid mt-2 mx-auto d-block" src="{{ asset("img/logos/documents.svg") }}" width="60px" height="60px">
	<p class="text-white h4">Documents</p>
	<p class="h5">{{ $Usager->nom . " " . $Usager->prénom }}</p>
</div>
@endsection

@section('onglet-bas-droit')
@if($Documents->nombre == 0)
<form action="{{ route('application.document.contact.sans-retrait') }}" method="post">
		@csrf
		<input type="hidden" name="idUsager" value="{{ $Usager->id }}">
		<button class="btn btn-lg btn-warning interface-onglet-bas-droit p-3" type="submit">Contact sans retrait de document</button>
	</form>
@endif
@endsection

@section('contenu')
<div class="col-9 offset-1 bg-success interface-main">
	<div class="pt-6 pb-2">
		<p class="h1 offset-1">Résultat de la recherche</p>
	</div>
	<div class="col-6 offset-2 pt-5 pb-2">
		<p class="h3">{{ $Documents->nombre }} documents trouvé.</p>
	</div>
	@include('includes.flash-div-class', ['div_class' => 'mt-4'])
	@if($Documents->nombre > 0)
	<div class="row py-2 px-2">
		<table class="table table-dark table-hover rounded-bottom">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Date de dépôt</th>
					<th scope="col">Agent dépositaire</th> 
					<th scope="col">Option</th>
				</tr>
			</thead>
			<tbody>
				@foreach($Documents as $Document)
				<tr>
					<th scope="row">{{ $Document->id }}</th>
					<td>{{ date('d-m-Y', strtotime($Document->dateDeDépôt)) }}</td>
					<td>{{ $Document->idAgentDépôt }}</td>
					<td>
						<button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#Retrait{{ $Document->id }}">
						Retirer
						</button>
						{{-- Modal de retrait --}}
						<div class="modal fade text-body" id="Retrait{{$Document->id}}" tabindex="-1" role="dialog">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content bg-dark text-white">
									<form action="{{ route('application.document.traitement.retrait', $Document->id) }}" method="get">
										@csrf
										<div class="modal-header">
											<h5 class="modal-title">Retrait d'un documents</h5>
										</div>
										<div class="modal-body">
											<p>Veuillez confirmer le retrait du document.</p>
										</div>
										<div class="modal-footer">
											
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
											<button class="btn btn-warning" type="submit">Retirer</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@endif
</div>
@endsection