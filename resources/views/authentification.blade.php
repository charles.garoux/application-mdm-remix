@extends('includes.template')

@section('contenu')
<form method="post" class="col-9 offset-1 pt-5 bg-danger justify-content-center interface-main">
	@csrf
	<div class="form-group col-6 offset-2 pt-5 pb-2">
		<input type="text" class="form-control form-control-lg" name="identifiant" id="identifiant" placeholder="Identifiant" maxlength="32" required/>
	</div>
	<div class="form-group col-6 offset-2  py-2">
		<input type="password" class="form-control form-control-lg" name="motDePasse" id="motDePasse" placeholder="Mot de passe" maxlength="32" required/>
	</div>
	<div class="row py-2">
		<button class="btn btn-lg btn-dark col-5 offset-5"><span class="h4">Connexion</span></button>
		@include('includes.flash-div-class', ['div_class' => 'mt-4 col-7 offset-4'])
	</div>
</form>
@endsection