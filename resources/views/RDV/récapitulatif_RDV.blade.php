@extends('includes.template')

@section('onglet-droit')
@if($Agent)
<div class="bg-primary interface-onglet-droit">
	<img class="img-fluid mt-4 mx-auto d-block" src="{{ asset("img/logos/avec-RDV.svg") }}" width="60px" height="60px">
	<p class=" text-white h4">Avec RDV</p>
</div>
@else
<div class="bg-warning interface-onglet-droit">
	<img class="img-fluid mt-4 mx-auto d-block" src="{{ asset("img/logos/sans-RDV.svg") }}" width="60px" height="60px">
	<p class=" text-white h4">Sans RDV</p>
</div>
@endif
@endsection

@section('contenu')
@if($Agent)
<div class="col-9 offset-1 pt-5 bg-primary interface-main">
	<p class="h2 mt-2 offset-2">Récapitulatif du rendez-vous</p>
	<div class="card mt-6 col-6 offset-3">
	  <div class="card-body">
	  	<p class="text-center h3">
	    	Rendez-vous avec<br>
	    	{{ $Agent->civilité . " " . $Agent->nom }}<br>
	    	{{ date('d/m H:i', strtotime($RDV->dateHeure)) }}<br>
	    	<br>
	    	pour {{ $Usager->civilité . " " . $Usager->nom }}
	    </p>
	  </div>
	</div>
</div>
@else
<div class="col-9 offset-1 pt-5 bg-warning interface-main">
	<p class="h2 mt-2 offset-2">Récapitulatif du rendez-vous</p>
	<div class="card mt-6 col-6 offset-3">
	  <div class="card-body">
		<p class="text-center h3">
	    	Rendez-vous AIS
	    	{{ date('d/m H:i', strtotime($RDV->dateHeure)) }}<br>
	    	<br>
	    	pour {{ $Usager->civilité . " " . $Usager->nom }}
	    </p>
	  </div>
	</div>
</div>
@endif

@endsection