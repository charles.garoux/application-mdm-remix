@extends('includes.template')

@section('onglet-droit')
<div class="bg-primary interface-onglet-droit">
	<img class="img-fluid mt-4 mx-auto d-block" src="{{ asset("img/logos/sans-RDV.svg") }}" width="60px" height="60px">
	<p class=" text-white h4">Recherche RDV</p>
</div>
@endsection

@section('contenu')
<form method="post" class="col-9 offset-1 pt-5 bg-primary justify-content-center interface-main">
	@csrf
	<div class="form-group col-6 offset-2 pt-4">
		<label for="dateDébut">Date de début de disponibilité</label>
		<input type="date" class="form-control form-control-lg" name="dateDébut" id="dateDébut" required/>
	</div>
	<div class="form-group col-6 offset-2 pt-4">
		<label for="dateFin">Date de fin disponibilité</label>
		<input type="date" class="form-control form-control-lg" name="dateFin" id="dateHFin" required/>
	</div>
	<div class="row py-2">
		<button class="btn btn-lg btn-dark col-5 offset-5"><span class="h4">Rechercher les créneaux</span></button>
		@include('includes.flash-div-class', ['div_class' => 'mt-4 col-7 offset-4'])
	</div>
</form>
@endsection