@extends('includes.template')

@section('onglet-droit')
<div class="bg-primary interface-onglet-droit">
	<img class="img-fluid mt-4 mx-auto d-block" src="{{ asset("img/logos/avec-RDV.svg") }}" width="60px" height="60px">
	<p class=" text-white h4">Avec RDV</p>
</div>
@endsection

@section('contenu')
<div method="post" class="col-9 offset-1 pt-5 bg-primary justify-content-center interface-main">
	<p class="col-9 offset-1 text-center text-white h3 mt-8">Arrivée confirmé.</p>
	<p class="col-9 offset-1 text-center text-white h3">Une notification à été envoyé à l'agent {{ $Agent->nom . " " . $Agent->prénom }}.</p>
	
</div>
@endsection