@extends('includes.template')

@section('onglet-droit')
<div class="bg-primary interface-onglet-droit">
	<img class="img-fluid mt-4 mx-auto d-block" src="{{ asset("img/logos/avec-RDV.svg") }}" width="60px" height="60px">
	<p class=" text-white h4">Avec RDV</p>
</div>
@endsection

@section('contenu')
<form method="post" class="col-9 offset-1 pt-5 bg-primary justify-content-center interface-main">
	@csrf
	<div class="form-group col-5 offset-2 pt-5">
		<input type="text" class="form-control form-control-lg" name="nom" id="nom" placeholder="Nom" maxlength="32" required/>
	</div>
	<div class="form-group col-5 offset-2">
		<input type="text" class="form-control form-control-lg" name="prénom" id="prénom" placeholder="Prénom" maxlength="32" required/>
	</div>
	<div class="form-group col-5 offset-2 pt-4">
		<input type="time" class="form-control form-control-lg pr-0" name="heure" id="heure" value="{{ now()->format('H:i') }}"  required/>
	</div>
	<button class="btn btn-lg btn-dark interface-onglet-bas-droit p-3"><span class="h4">Rechercher le rendez-vous</span></button>
	@include('includes.flash-div-class', ['div_class' => 'mt-4 col-7 offset-4'])
</form>
@endsection