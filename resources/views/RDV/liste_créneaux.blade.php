@extends('includes.template')
@section('onglet-droit')
@if($type == "AIS")
<div class="bg-warning interface-onglet-droit">
	<img class="img-fluid mt-4 mx-auto d-block" src="{{ asset("img/logos/sans-RDV.svg") }}" width="60px" height="60px">
	<p class=" text-white h4">Prise d'AIS</p>
</div>
@else
<div class="bg-primary interface-onglet-droit">
	<img class="img-fluid mt-4 mx-auto d-block" src="{{ asset("img/logos/sans-RDV.svg") }}" width="60px" height="60px">
	<p class=" text-white h4">Prise de RDV</p>
</div>
@endif
@endsection

@section('contenu')
@if($type == "AIS")
<div class="col-9 offset-1 pt-5 bg-warning interface-main">
@else
<div class="col-9 offset-1 pt-5 bg-primary interface-main">
@endif
	<div class="pt-6 pb-2">
		<p class="h1 offset-1">Résultat de la recherche</p>
	</div>
	<div class="col-6 offset-2 pt-5 pb-2">
		<p class="h3">{{ $RDVs->nombre }} rendez-vous trouvé.</p>
	</div>
	@include('includes.flash-div-class', ['div_class' => 'mt-4'])
	@if($RDVs->nombre > 0)
	<div class="row py-2 px-2">
		<table class="table table-dark table-hover rounded-bottom">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Date</th>
					<th scope="col">Heure</th>
					@if ($type == "RDV")
					<th scope="col">Agent</th>
					@endif
					<th scope="col">Option</th>
				</tr>
			</thead>
			<tbody>
				@foreach($RDVs as $RDV)
				<tr>
					<th scope="row">{{ $RDV->id }}</th>
					<td>{{ date('d-m-Y', strtotime($RDV->dateHeure)) }}</td>
					<td>{{ date('H:i', strtotime($RDV->dateHeure)) }}</td>
					@if ($type == "RDV")
					<td>{{ $RDV->idAgent }}</td>
					@endif
					<td>
						<button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#RDV{{ $RDV->id }}">
						Selectionner
						</button>
						{{-- Modal de prise de RDV --}}
						<div class="modal fade text-body" id="RDV{{$RDV->id}}" tabindex="-1" role="dialog">
							<div class="modal-dialog modal-lg modal-dialog-centered" role="RDV">
								<div class="modal-content bg-dark text-white">
									<form action="{{ route('application.RDV.prise') }}" method="post">
										@csrf
										<input type="hidden" name="idRDV" value="{{$RDV->id}}">
										<div class="modal-header">
											<h5 class="modal-title">Prise de RDV</h5>
										</div>
										<div class="modal-body row">
											<p class="col-6 text-center">Date : {{ date('d-m-Y', strtotime($RDV->dateHeure)) }}</p>
											<p class="col-6 text-center">Heure : {{ date('H:i', strtotime($RDV->dateHeure)) }}</p>
											@if (Session::has("Usager"))
												<div class="form-group col-2">
													<select type="text" class="form-control form-control-lg" name="civilité" id="civilité">
														<option value="{{ Session::get("Usager")->civilité }}">{{ Session::get("Usager")->civilité }}</option>
														<option value="M">M</option>
														<option value="M">Mme</option>
													</select>
												</div>
												<div class="form-group col-5">
													<input type="text" class="form-control form-control-lg" name="nom" id="nom" placeholder="Nom" value="{{ Session::get("Usager")->nom }}" maxlength="32" required/>
												</div>
												<div class="form-group col-5">
													<input type="text" class="form-control form-control-lg" name="prénom" id="prénom" placeholder="Prénom" maxlength="32" value="{{ Session::get("Usager")->prénom }}" required/>
												</div>
												<div class="form-group col-6">
													<input type="tel" class="form-control form-control-lg" name="numéroDeTéléphone" id="numéroDeTéléphone" placeholder="06 ** ** ** **" value="{{ Session::get("Usager")->numéroDeTéléphone }}" maxlength="10"/>
												</div>
												<div class="form-group col-6">
													<input type="email" class="form-control form-control-lg" name="email" id="email" placeholder="email{{"@"}}exemple.ex" value="{{ Session::get("Usager")->email }}" maxlength="32"/>
												</div>
											@else
												<div class="form-group col-2">
													<select type="text" class="form-control form-control-lg" name="civilité" id="civilité">
														<option value=""></option>
														<option value="M">M</option>
														<option value="M">Mme</option>
													</select>
												</div>
												<div class="form-group col-5">
													<input type="text" class="form-control form-control-lg" name="nom" id="nom" placeholder="Nom" maxlength="32" required/>
												</div>
												<div class="form-group col-5">
													<input type="text" class="form-control form-control-lg" name="prénom" id="prénom" placeholder="Prénom" maxlength="32" required/>
												</div>
												<div class="form-group col-6">
													<input type="text" class="form-control form-control-lg" name="numéroDeTéléphone" id="numéroDeTéléphone" placeholder="06 ** ** ** **" maxlength="10"/>
												</div>
												<div class="form-group col-6">
													<input type="email" class="form-control form-control-lg" name="email" id="email" placeholder="email{{"@"}}exemple.ex" maxlength="32"/>
												</div>
											@endif
										</div>
										<div class="modal-footer">
											<div class="form-group form-check">
												<input id="check_anonyme" type="checkbox" class="form-check-input" id="anonyme">
												<label class="form-check-label" for="check_anonyme">Anonyme</label>
											</div>
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
											<button class="btn btn-warning" type="submit">Prendre RDV</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@endif
</div>
<script>
	window.onload = function()
	{
		const anonyme = document.getElementById("check_anonyme");

		anonyme.onclick = function()
		{
			if (anonyme.checked)
			{
				document.getElementById('nom').value = 'Anonyme';			
				document.getElementById('prénom').value = 'Anonyme';
			}
			else
			{
				document.getElementById('nom').value = '';			
				document.getElementById('prénom').value = '';	
			}
		}
	}
</script>





@endsection