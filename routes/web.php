<?php

Route::get('authentification', 'AuthentificationAgentController@formulaire')->name("application.agent.authentification.formulaire");
Route::post('authentification', 'AuthentificationAgentController@vérification')->name("application.agent.authentification.vérification");
Route::get('déconnexion', 'AuthentificationAgentController@déconnexion')->name("application.agent.déconnexion");


Route::group(["middleware" => 'AuthentificationAgent'], function() {

	Route::get('/', function () { return view('menu'); })->name("application.agent.volant.menu");;

	//=====================================Dépot/retrait Document ==================================

	Route::get("/menu-document", "Agent\DocumentController@menu")->name("application.document.menu");

	Route::get("/dépôt-document", "Agent\DocumentController@formulaire_dépôt")->name("application.document.formulaire.dépôt");
	Route::post("/dépôt-document", "Agent\DocumentController@traitement_dépôt")->name("application.document.traitement.dépôt");

	Route::get("/recherche-document", "Agent\DocumentController@formulaire_recherche")->name("application.document.formulaire.recherche");
	Route::post("/recherche-document", "Agent\DocumentController@redirection_recherche")->name("application.document.redirection.recherche");

	Route::get("/document/{nom}-{prenom}", "Agent\DocumentController@liste_document")->name("application.document.usager.liste");
	Route::get("/retirer-document/{idDocument}", "Agent\DocumentController@retrait_document")->name("application.document.traitement.retrait");

	Route::post("/contact-document-sans-retrait", "Agent\DocumentController@contact_document_sans_retrait")->name("application.document.contact.sans-retrait");

	//============================================RDV================================================================

	Route::get("/recherche-RDV", "Agent\RDVController@formulaire_recherche_RDV_usager")->name("application.RDV.formulaire.recherche.usager");
	Route::post("/recherche-RDV", "Agent\RDVController@traitement_recherche")->name("application.RDV.redirection.recherche");

	Route::get("/recherche_créneaux_RDV", "Agent\RDVController@formulaire_recherche_RDV")->name("application.RDV.créneaux.recherche.formulaire");
	Route::post("/recherche_créneaux_RDV", "Agent\RDVController@traitement_recherche_RDV")->name("application.RDV.créneaux.recherche.traitement");

	Route::get("/liste_créneaux_RDV/{dateDebut}_{dateFin}_{type}", "Agent\RDVController@liste_créneaux")->name("application.RDV.créneaux.liste");
	Route::post("/prise_de_RDV", "Agent\RDVController@prise_de_RDV")->name("application.RDV.prise");

	Route::get("/récapitulatif_RDV/{idRDV}-{type}", "Agent\RDVController@récapitulatif_RDV")->name("application.RDV.récapitulatif");

	//============================================Administration (pour démonstration) ================================================================

	Route::get("/administration", "Administration\Démonstration@liste_formulaire")->name('application.administration.liste.formulaire');
	Route::post("/administration/création/RDV", "Administration\Démonstration@ajouter_RDV")->name('application.administration.ajout.RDV');
	Route::post("/administration/création/agent", "Administration\Démonstration@ajouter_agent")->name('application.administration.ajout.agent');
	Route::post("/administration/création/usager", "Administration\Démonstration@ajouter_usager")->name('application.administration.ajout.usager');
	Route::post("/administration/création/MDM", "Administration\Démonstration@ajouter_MDM")->name('application.administration.ajout.MDM');

});